const express = require('express');
const pmx = require('pmx');
const pm2 = require('pm2');
const exec = require('child_process').exec;

pmx.initModule({}, function (err, conf) {
    pm2.connect(function (err2) {
        if (err || err2) {
            console.error(err || err2);
            return process.exit(1);
        }

        new Worker(conf);
    });
});

const Worker = function (conf) {
    this.tokens = conf.tokens;
    this.port = conf.port;

    if (typeof (this.tokens) !== 'object') this.tokens = JSON.parse(this.tokens);

    let app = express();

    app.post('/:app', (req, res) => {
        let token = req.header('x-gitlab-token');

        if (!this.tokens[req.params.app]) return res.json({ err: 'No app defined.' });

        if (this.tokens[req.params.app] !== token) return res.json({ err: 'Invalid token' });
        res.json({ status: 'OK' });

        pm2.describe(req.params.app, function (err, apps) {
            if (err || !apps || apps.length === 0) return cb(err || new Error('Application not found'));

            const cwd = apps[0].pm_cwd ? apps[0].pm_cwd : apps[0].pm2_env.pm_cwd;

            exec('git pull origin master', { cwd }, (err) => {
                if (err) throw err;

                pm2.gracefulReload(req.params.app);
            })
        });
    });

    app.listen(this.port, () => {
        console.log('Listening on port :' + this.port);
    });
};
